TEMPLATE = subdirs
SUBDIRS += iTestServer iTestClient

QMAKE_CFLAGS += -std=c++11 -fno-permissivei
QMAKE_CXXFLAGS += -std=c++11 -fno-permissive
QMAKE_LFLAGS += -std=c++11 -fno-permissive
QT += sql
CONFIG += console

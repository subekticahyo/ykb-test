/****************************************************************************
** Meta object code from reading C++ file 'session_wizard.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../session_wizard.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'session_wizard.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SessionWizard[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      23,   14,   14,   14, 0x08,
      31,   14,   14,   14, 0x08,
      52,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SessionWizard[] = {
    "SessionWizard\0\0match()\0split()\0"
    "toggleMatchEnabled()\0toggleSplitEnabled()\0"
};

void SessionWizard::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SessionWizard *_t = static_cast<SessionWizard *>(_o);
        switch (_id) {
        case 0: _t->match(); break;
        case 1: _t->split(); break;
        case 2: _t->toggleMatchEnabled(); break;
        case 3: _t->toggleSplitEnabled(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SessionWizard::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SessionWizard::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_SessionWizard,
      qt_meta_data_SessionWizard, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SessionWizard::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SessionWizard::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SessionWizard::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SessionWizard))
        return static_cast<void*>(const_cast< SessionWizard*>(this));
    return QDialog::qt_metacast(_clname);
}

int SessionWizard::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

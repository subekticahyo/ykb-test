/****************************************************************************
** Meta object code from reading C++ file 'mtadvancedgroupbox.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mtadvancedgroupbox.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mtadvancedgroupbox.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MTAdvancedGroupBox[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      28,   20,   19,   19, 0x0a,
      51,   45,   19,   19, 0x0a,
      79,   69,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MTAdvancedGroupBox[] = {
    "MTAdvancedGroupBox\0\0checked\0"
    "setChecked(bool)\0title\0setTitle(QString)\0"
    "statustip\0setStatusTip(QString)\0"
};

void MTAdvancedGroupBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MTAdvancedGroupBox *_t = static_cast<MTAdvancedGroupBox *>(_o);
        switch (_id) {
        case 0: _t->setChecked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->setTitle((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->setStatusTip((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MTAdvancedGroupBox::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MTAdvancedGroupBox::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MTAdvancedGroupBox,
      qt_meta_data_MTAdvancedGroupBox, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MTAdvancedGroupBox::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MTAdvancedGroupBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MTAdvancedGroupBox::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MTAdvancedGroupBox))
        return static_cast<void*>(const_cast< MTAdvancedGroupBox*>(this));
    return QWidget::qt_metacast(_clname);
}

int MTAdvancedGroupBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

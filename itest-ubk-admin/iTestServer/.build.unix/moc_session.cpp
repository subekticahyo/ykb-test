/****************************************************************************
** Meta object code from reading C++ file 'session.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../session.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'session.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Session[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x0a,
      34,    8,   26,    8, 0x0a,
      41,    8,    8,    8, 0x0a,
      72,    8,    8,    8, 0x0a,
      95,    8,   26,    8, 0x0a,
     124,    8,  114,    8, 0x0a,
     142,  135,    8,    8, 0x0a,
     191,    8,  187,    8, 0x0a,
     216,    8,  207,    8, 0x0a,
     230,    8,    8,    8, 0x0a,
     242,    8,    8,    8, 0x0a,
     263,    8,  187,    8, 0x0a,
     286,    8,  277,    8, 0x0a,
     311,    8,  299,    8, 0x0a,
     326,    8,  299,    8, 0x0a,
     334,    8,  187,    8, 0x0a,
     344,    8,    8,    8, 0x0a,
     375,    8,  366,    8, 0x0a,
     386,    8,    8,    8, 0x0a,
     408,    8,    8,    8, 0x0a,
     454,    8,  440,    8, 0x0a,
     470,    8,   26,    8, 0x0a,
     489,    8,  484,    8, 0x0a,
     502,    8,  484,    8, 0x0a,
     515,    8,    8,    8, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Session[] = {
    "Session\0\0setName(QString)\0QString\0"
    "name()\0setDateTimeFromString(QString)\0"
    "setDateTime(QDateTime)\0dateTimeToString()\0"
    "QDateTime\0dateTime()\0,,,,,,\0"
    "addLogEntry(int,int,int,int,int,int,QString)\0"
    "int\0numLogEntries()\0LogEntry\0logEntry(int)\0"
    "deleteLog()\0addStudent(Student*)\0"
    "numStudents()\0Student*\0student(int)\0"
    "long double\0maximumScore()\0score()\0"
    "average()\0setPassMark(PassMark)\0"
    "PassMark\0passMark()\0loadPassMark(QString)\0"
    "setScoringSystem(ScoringSystem)\0"
    "ScoringSystem\0scoringSystem()\0"
    "sessionData()\0bool\0mostPassed()\0"
    "isArchived()\0destruct()\0"
};

void Session::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Session *_t = static_cast<Session *>(_o);
        switch (_id) {
        case 0: _t->setName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: { QString _r = _t->name();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 2: _t->setDateTimeFromString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->setDateTime((*reinterpret_cast< QDateTime(*)>(_a[1]))); break;
        case 4: { QString _r = _t->dateTimeToString();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 5: { QDateTime _r = _t->dateTime();
            if (_a[0]) *reinterpret_cast< QDateTime*>(_a[0]) = _r; }  break;
        case 6: _t->addLogEntry((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6])),(*reinterpret_cast< const QString(*)>(_a[7]))); break;
        case 7: { int _r = _t->numLogEntries();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 8: { LogEntry _r = _t->logEntry((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< LogEntry*>(_a[0]) = _r; }  break;
        case 9: _t->deleteLog(); break;
        case 10: _t->addStudent((*reinterpret_cast< Student*(*)>(_a[1]))); break;
        case 11: { int _r = _t->numStudents();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 12: { Student* _r = _t->student((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< Student**>(_a[0]) = _r; }  break;
        case 13: { long double _r = _t->maximumScore();
            if (_a[0]) *reinterpret_cast< long double*>(_a[0]) = _r; }  break;
        case 14: { long double _r = _t->score();
            if (_a[0]) *reinterpret_cast< long double*>(_a[0]) = _r; }  break;
        case 15: { int _r = _t->average();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 16: _t->setPassMark((*reinterpret_cast< PassMark(*)>(_a[1]))); break;
        case 17: { PassMark _r = _t->passMark();
            if (_a[0]) *reinterpret_cast< PassMark*>(_a[0]) = _r; }  break;
        case 18: _t->loadPassMark((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 19: _t->setScoringSystem((*reinterpret_cast< ScoringSystem(*)>(_a[1]))); break;
        case 20: { ScoringSystem _r = _t->scoringSystem();
            if (_a[0]) *reinterpret_cast< ScoringSystem*>(_a[0]) = _r; }  break;
        case 21: { QString _r = _t->sessionData();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 22: { bool _r = _t->mostPassed();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 23: { bool _r = _t->isArchived();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 24: _t->destruct(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Session::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Session::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Session,
      qt_meta_data_Session, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Session::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Session::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Session::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Session))
        return static_cast<void*>(const_cast< Session*>(this));
    return QObject::qt_metacast(_clname);
}

int Session::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'student.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../student.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'student.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Student[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x0a,
      34,    8,   26,    8, 0x0a,
      41,    8,    8,    8, 0x0a,
      60,    8,   56,    8, 0x0a,
      69,    8,   56,    8, 0x0a,
     109,    8,  102,    8, 0x0a,
     117,    8,  102,    8, 0x0a,
     132,    8,    8,    8, 0x0a,
     152,    8,  147,    8, 0x0a,
     162,    8,    8,    8, 0x0a,
     204,    8,    8,    8, 0x0a,
     261,    8,  231,    8, 0x0a,
     271,    8,    8,    8, 0x0a,
     287,    8,  147,    8, 0x0a,
     296,    8,   26,    8, 0x0a,
     310,    8,   26,    8, 0x0a,
     331,    8,  147,    8, 0x0a,
     356,  354,  349,    8, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Student[] = {
    "Student\0\0setName(QString)\0QString\0"
    "name()\0setNumber(int)\0int\0number()\0"
    "numCorrectAnswers(ScoringSystem)\0"
    "double\0score()\0maximumScore()\0"
    "setReady(bool)\0bool\0isReady()\0"
    "setResults(QMap<QString,QuestionAnswer>*)\0"
    "updateScore(ScoringSystem)\0"
    "QMap<QString,QuestionAnswer>*\0results()\0"
    "setPassed(bool)\0passed()\0studentData()\0"
    "studentArchiveData()\0wasAsked(QString)\0"
    "uint\0,\0replaceOccurrences(QString,QString)\0"
};

void Student::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Student *_t = static_cast<Student *>(_o);
        switch (_id) {
        case 0: _t->setName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: { QString _r = _t->name();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 2: _t->setNumber((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: { int _r = _t->number();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 4: { int _r = _t->numCorrectAnswers((*reinterpret_cast< ScoringSystem(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 5: { double _r = _t->score();
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = _r; }  break;
        case 6: { double _r = _t->maximumScore();
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = _r; }  break;
        case 7: _t->setReady((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: { bool _r = _t->isReady();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 9: _t->setResults((*reinterpret_cast< QMap<QString,QuestionAnswer>*(*)>(_a[1]))); break;
        case 10: _t->updateScore((*reinterpret_cast< ScoringSystem(*)>(_a[1]))); break;
        case 11: { QMap<QString,QuestionAnswer>* _r = _t->results();
            if (_a[0]) *reinterpret_cast< QMap<QString,QuestionAnswer>**>(_a[0]) = _r; }  break;
        case 12: _t->setPassed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: { bool _r = _t->passed();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 14: { QString _r = _t->studentData();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 15: { QString _r = _t->studentArchiveData();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 16: { bool _r = _t->wasAsked((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 17: { uint _r = _t->replaceOccurrences((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< uint*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Student::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Student::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Student,
      qt_meta_data_Student, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Student::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Student::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Student::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Student))
        return static_cast<void*>(const_cast< Student*>(this));
    return QObject::qt_metacast(_clname);
}

int Student::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

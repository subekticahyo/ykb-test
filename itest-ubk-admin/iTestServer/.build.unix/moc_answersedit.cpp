/****************************************************************************
** Meta object code from reading C++ file 'answersedit.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../answersedit.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'answersedit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AnswerEdit[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      34,   11,   29,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_AnswerEdit[] = {
    "AnswerEdit\0\0setVisible(bool)\0bool\0"
    "isVisible()\0"
};

void AnswerEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AnswerEdit *_t = static_cast<AnswerEdit *>(_o);
        switch (_id) {
        case 0: _t->setVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: { bool _r = _t->isVisible();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AnswerEdit::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AnswerEdit::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_AnswerEdit,
      qt_meta_data_AnswerEdit, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AnswerEdit::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AnswerEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AnswerEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AnswerEdit))
        return static_cast<void*>(const_cast< AnswerEdit*>(this));
    return QWidget::qt_metacast(_clname);
}

int AnswerEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_AnswersEdit[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,
      25,   12,   12,   12, 0x0a,
      47,   12,   12,   12, 0x0a,
      65,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_AnswersEdit[] = {
    "AnswersEdit\0\0addAnswer()\0removeAnswer(QString)\0"
    "removeAnswer(int)\0clear()\0"
};

void AnswersEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AnswersEdit *_t = static_cast<AnswersEdit *>(_o);
        switch (_id) {
        case 0: _t->addAnswer(); break;
        case 1: _t->removeAnswer((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->removeAnswer((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->clear(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AnswersEdit::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AnswersEdit::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_AnswersEdit,
      qt_meta_data_AnswersEdit, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AnswersEdit::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AnswersEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AnswersEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AnswersEdit))
        return static_cast<void*>(const_cast< AnswersEdit*>(this));
    return QWidget::qt_metacast(_clname);
}

int AnswersEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

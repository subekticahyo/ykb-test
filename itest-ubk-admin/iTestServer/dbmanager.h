#ifndef DBMANAGER_H
#define DBMANAGER_H
#include"class.h"
#include<QString>
#include<QtSql/QSqlDatabase>

class DbManager
{
    public:
        DbManager(const QString& dbhost,const qint32& dbport);
        bool connect(const QString& dbname, const QString& dbusername,const QString& dbpassword);
        bool disconnect();

        //CLASS-RELATED
        bool addClass(const Class& kelas);
        bool updateClassByID(const Class& kelas, const qint32& value);
        bool updateClassByColumn(const Class& kelas, const QString& col, const qint32& value);
        bool removeClassAll();
        bool removeClassByID( const qint32& id);
        Class * getClassByID(const qint32& id);
        QMap<qint32 *,Class *> getClassAll();
        QMap<qint32 *,Class *> getClassByColumn(const QString& column,const qint32& value,const QString& operation);

        //CLASS-MEMBER-RELATED
        bool addClassMember(const ClassMember& kelas);
        bool updateClassMemberByID(const ClassMember& kelas, const qint32& value);
        bool updateClassMemberByColumn(const ClassMember& kelas, const QString& col, const qint32& value);
        bool removeClassMemberAll();
        bool removeClassMemberByID( const qint32& id);
        ClassMember* getClassMemberByID(const qint32& id);
        QMap<qint32 *, ClassMember*> getClassMemberAll();
        QMap<qint32 *, ClassMember*> getClassMemberByColumn(const QString& column,const qint32& value,const QString& operation);


        //QUESTION-RELATED
        bool addQuestion(const Question& kelas);
        bool updateQuestionByID(const Question& kelas, const qint32& value);
        bool updateQuestionByColumn(const Question& kelas, const QString& col, const qint32& value);
        bool removeQuestionAll();
        bool removeQuestionByID( const qint32& id);
        Question getQuestionByID(const qint32& id);
        QMap<qint32*,Question*> getQuestionAll();
        QMap<qint32*,Question*> getQuestionByColumn(const QString& column,const qint32& value,const QString& operation);


        //QUESTIONANSWER-RELATED
        bool addQuestionAnswer(const QuestionAnswer& kelas);
        bool updateQuestionAnswerByID(const QuestionAnswer& kelas, const qint32& value);
        bool updateQuestionAnswerByColumn(const QuestionAnswer& kelas, const QString& col, const qint32& value);
        bool removeQuestionAnswerAll();
        bool removeQuestionAnswerByID( const qint32& id);
        QuestionAnswer getQuestionAnswerByID(const qint32& id);
        QMap<qint32*,QuestionAnswer*> getQuestionAnswerAll();
        QMap<qint32*,QuestionAnswer*> getQuestionAnswerByColumn(const QString& column,const qint32& value,const QString& operation);


        //QUESTIONITEM-RELATED
        bool addQuestionItem(const QuestionItem& kelas);
        bool updateQuestionItemByID(const QuestionItem& kelas, const qint32& value);
        bool updateQuestionItemByColumn(const QuestionItem& kelas, const QString& col, const qint32& value);
        bool removeQuestionItemAll();
        bool removeQuestionItemByID( const qint32& id);
        QuestionItem getQuestionItemByID(const qint32& id);
        QMap<qint32*,QuestionItem*> getQuestionItemAll();
        QMap<qint32*,QuestionItem*> getQuestionItemByColumn(const QString& column,const qint32& value,const QString& operation);

        //STUDENT-RELATED
        bool addStudent(const Student& kelas);
        bool updateStudentByID(const Student& kelas, const qint32& value);
        bool updateStudentByColumn(const Student& kelas, const QString& col, const qint32& value);
        bool removeStudentAll();
        bool removeStudentByID( const qint32& id);
        bool getStudentAll();
        bool getStudentByID(const qint32& id);
        bool getStudentByColumn(const QString& column,const qint32& value,const QString& operation);

        //SESSION-RELATED
        bool addSession(const Session& kelas);
        bool updateSessionByID(const Session& kelas, const qint32& value);
        bool updateSessionByColumn(const Session& kelas, const QString& col, const qint32& value);
        bool removeSessionAll();
        bool removeSessionByID( const qint32& id);
        Session* getSessionByID(const qint32& id);
        QMap<qint32*, Session*> getSessionAll();
        QMap<qint32*, Session*> getSessionByColumn(const QString& column,const qint32& value,const QString& operation);


        //SESSIONENTRY-RELATED
        bool addSessionEntry(const SessionEntry& kelas);
        bool updateSessionEntryByID(const SessionEntry& kelas, const qint32& value);
        bool updateSessionEntryByColumn(const SessionEntry& kelas, const QString& col, const qint32& value);
        bool removeSessionEntryAll();
        bool removeSessionEntryByID( const qint32& id);
        SessionEntry* getSessionEntryByID(const qint32& id);
        QMap<qint32*, SessionEntry*> getSessionEntryAll();
        QMap<qint32*, SessionEntry*> getSessionEntryByColumn(const QString& column,const qint32& value,const QString& operation);


        //UTIL
        void setCurrentTable(const QString &table);
        QString  getCurrentTable();
        void setCurrentColumn(const QString &column);
        QString getCurrentColumn();
        void setCurrentValue(const QString &value);
        QString getCurrentValue();
        void setCurrentId(const QString &id);
        QString getCurrentId();

    private:
        QSqlDatabase m_db;
        QString dbhost;
        qint32  dbport;
        QString dbusername;
        QString dbpassword;
        QString dbname;

        QString currentTable;
        QString currentColumn;
        QString currentValue;
        QString currentId;

};

#endif // DBMANAGER_H

#include "dbmanager.h"

/*

DbManager::DbManager(const QString &dbhost, const qint32 &dbport)
{
    qDebug("-- db initialized --");
    m_db.setHostName(dbhost);
    m_db.setPort(dbport);
}

bool DbManager::connect(const QString &dbname, const QString &dbusername, const QString &dbpassword){
    m_db.setUserName(dbusername);
    m_db.setPassword(dbpassword);
    m_db.setDatabaseName(dbname);

    if(!m_db.isOpen()){
        return m_db.open();
        qDebug("-- db connected --");
    }else{
        return false;
        qDebug("-- db failed to connected --");
    }
}

void DbManager::disconnect(){
    if(m_db.isOpen()){
        m_db.close();
        qDebug("-- db closed --");
    }
}


//CLASS-RELATED
bool DbManager::addClass(const Class& kelas){

}
bool DbManager::updateClassByID(const Class& kelas, const qint32& value){

}
bool DbManager::updateClassByColumn(const Class& kelas, const QString& col, const qint32& value){

}
bool DbManager::removeClassAll(){}
bool DbManager::removeClassByID( const qint32& id){}
Class * DbManager::getClassByID(const qint32& id){}
QMap<qint32 *,Class *> DbManager::getClassAll(){}
QMap<qint32 *,Class *> DbManager::getClassByColumn(const QString& column,const qint32& value,const QString& operation){}

//CLASS-MEMBER-RELATED
bool DbManager::addClassMember(const ClassMember& kelas){}
bool DbManager::updateClassMemberByID(const ClassMember& kelas, const qint32& value){}
bool DbManager::updateClassMemberByColumn(const ClassMember& kelas, const QString& col, const qint32& value){}
bool DbManager::removeClassMemberAll(){}
bool DbManager::removeClassMemberByID( const qint32& id){}
ClassMember* DbManager::getClassMemberByID(const qint32& id){}
QMap<qint32 *, ClassMember*> DbManager::getClassMemberAll(){}
QMap<qint32 *, ClassMember*> DbManager::getClassMemberByColumn(const QString& column,const qint32& value,const QString& operation){}


//QUESTION-RELATED
bool DbManager::addQuestion(const Question& kelas){}
bool DbManager::updateQuestionByID(const Question& kelas, const qint32& value){}
bool DbManager::updateQuestionByColumn(const Question& kelas, const QString& col, const qint32& value){}
bool DbManager::removeQuestionAll(){}
bool DbManager::removeQuestionByID( const qint32& id){}
Question DbManager::getQuestionByID(const qint32& id){}
QMap<qint32*,Question*> DbManager::getQuestionAll(){}
QMap<qint32*,Question*> DbManager::getQuestionByColumn(const QString& column,const qint32& value,const QString& operation){}


//QUESTIONANSWER-RELATED
bool DbManager::addQuestionAnswer(const QuestionAnswer& kelas){}
bool DbManager::updateQuestionAnswerByID(const QuestionAnswer& kelas, const qint32& value){}
bool DbManager::updateQuestionAnswerByColumn(const QuestionAnswer& kelas, const QString& col, const qint32& value){}
bool DbManager::removeQuestionAnswerAll(){}
bool DbManager::removeQuestionAnswerByID( const qint32& id){}
QuestionAnswer DbManager::getQuestionAnswerByID(const qint32& id){}
QMap<qint32*,QuestionAnswer*> DbManager::getQuestionAnswerAll(){}
QMap<qint32*,QuestionAnswer*> DbManager::getQuestionAnswerByColumn(const QString& column,const qint32& value,const QString& operation){}


//QUESTIONITEM-RELATED
bool DbManager::addQuestionItem(const QuestionItem& kelas){}
bool DbManager::updateQuestionItemByID(const QuestionItem& kelas, const qint32& value){}
bool DbManager::updateQuestionItemByColumn(const QuestionItem& kelas, const QString& col, const qint32& value){}
bool DbManager::removeQuestionItemAll(){}
bool DbManager::removeQuestionItemByID( const qint32& id){}
QuestionItem DbManager::getQuestionItemByID(const qint32& id){}
QMap<qint32*,QuestionItem*> DbManager::getQuestionItemAll(){}
QMap<qint32*,QuestionItem*> DbManager::getQuestionItemByColumn(const QString& column,const qint32& value,const QString& operation){}

//STUDENT-RELATED
bool DbManager::addStudent(const Student& kelas){}
bool DbManager::updateStudentByID(const Student& kelas, const qint32& value){}
bool DbManager::updateStudentByColumn(const Student& kelas, const QString& col, const qint32& value){}
bool DbManager::removeStudentAll(){}
bool DbManager::removeStudentByID( const qint32& id){}
bool DbManager::getStudentAll(){}
bool DbManager::getStudentByID(const qint32& id){}
bool DbManager::getStudentByColumn(const QString& column,const qint32& value,const QString& operation){}

//SESSION-RELATED
bool DbManager::addSession(const Session& kelas){}
bool DbManager::updateSessionByID(const Session& kelas, const qint32& value){}
bool DbManager::updateSessionByColumn(const Session& kelas, const QString& col, const qint32& value){}
bool DbManager::removeSessionAll(){}
bool DbManager::removeSessionByID( const qint32& id){}
Session* DbManager::getSessionByID(const qint32& id){}
QMap<qint32*, Session*> DbManager::getSessionAll(){}
QMap<qint32*, Session*> DbManager::getSessionByColumn(const QString& column,const qint32& value,const QString& operation){}


//SESSIONENTRY-RELATED
bool DbManager::addSessionEntry(const SessionEntry& kelas){

}
bool DbManager::updateSessionEntryByID(const SessionEntry& kelas, const qint32& value){

}
bool DbManager::updateSessionEntryByColumn(const SessionEntry& kelas, const QString& col, const qint32& value){

}
bool DbManager::removeSessionEntryAll(){

}
bool DbManager::removeSessionEntryByID( const qint32& id){

}
SessionEntry* DbManager::getSessionEntryByID(const qint32& id){

}
QMap<qint32*, SessionEntry*> DbManager::getSessionEntryAll(){

}
QMap<qint32*, SessionEntry*> DbManager::getSessionEntryByColumn(const QString& column,const qint32& value,const QString& operation){

}


//UTIL
void DbManager::setCurrentTable(const QString &table){
    currentTable = table;
}
QString  DbManager::getCurrentTable(){
    return currentTable;
}
void DbManager::setCurrentColumn(const QString &column){
    currentColumn = column;
}
QString DbManager::getCurrentColumn(){
    return currentColumn;
}
void DbManager::setCurrentValue(const QString &value){
    currentValue = value;
}
QString DbManager::getCurrentValue(){
    return currentValue;
}
void DbManager::setCurrentId(const QString &id){
    currentId = id;
}
QString DbManager::getCurrentId(){
    return currentId;
}

*/



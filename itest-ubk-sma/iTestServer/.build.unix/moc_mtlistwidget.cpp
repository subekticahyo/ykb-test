/****************************************************************************
** Meta object code from reading C++ file 'mtlistwidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mtlistwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mtlistwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MTListWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      67,   56,   52,   13, 0x0a,
     101,   99,   13,   13, 0x0a,
     125,  120,   13,   13, 0x0a,
     162,   99,  157,   13, 0x0a,
     185,  120,  157,   13, 0x0a,
     221,   13,   52,   13, 0x0a,
     255,   13,  238,   13, 0x0a,
     273,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MTListWidget[] = {
    "MTListWidget\0\0currentIndexAvailabilityChanged(bool)\0"
    "int\0le,keyword\0filterItems(QLineEdit*,QString)\0"
    "i\0highlightItem(int)\0item\0"
    "highlightItem(QListWidgetItem*)\0bool\0"
    "isItemHighlighted(int)\0"
    "isItemHighlighted(QListWidgetItem*)\0"
    "highlightedRow()\0QListWidgetItem*\0"
    "highlightedItem()\0"
    "emitCurrentIndexAvailabilityChanged()\0"
};

void MTListWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MTListWidget *_t = static_cast<MTListWidget *>(_o);
        switch (_id) {
        case 0: _t->currentIndexAvailabilityChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: { int _r = _t->filterItems((*reinterpret_cast< QLineEdit*(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 2: _t->highlightItem((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->highlightItem((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 4: { bool _r = _t->isItemHighlighted((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: { bool _r = _t->isItemHighlighted((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { int _r = _t->highlightedRow();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 7: { QListWidgetItem* _r = _t->highlightedItem();
            if (_a[0]) *reinterpret_cast< QListWidgetItem**>(_a[0]) = _r; }  break;
        case 8: _t->emitCurrentIndexAvailabilityChanged(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MTListWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MTListWidget::staticMetaObject = {
    { &QListWidget::staticMetaObject, qt_meta_stringdata_MTListWidget,
      qt_meta_data_MTListWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MTListWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MTListWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MTListWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MTListWidget))
        return static_cast<void*>(const_cast< MTListWidget*>(this));
    return QListWidget::qt_metacast(_clname);
}

int MTListWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QListWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void MTListWidget::currentIndexAvailabilityChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

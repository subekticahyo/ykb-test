#include "dbmanager.h"
#include "sqlite_database.h"

DBManager::DBManager()
{
   sqlitedb = new SqliteDatabase();
}

void DBManager::initializeDB(){
    qDebug("-- DB INITIALIZED --");
    if(!checkTableExist("student")){ sqlitedb->execute("CREATE TABLE student (id INT PRIMARY KEY autoincrement, nik VARCHAR(16), name VARCHAR(50), status INT)"); }
    if(!checkTableExist("class")){ sqlitedb->execute("CREATE TABLE class (id INT PRIMARY KEY autoincrement, name VARCHAR(50), level INT, status INT)"); }
    if(!checkTableExist("flag")){ sqlitedb->execute("CREATE TABLE flag (id INT PRIMARY KEY autoincrement, name VARCHAR(50), status INT)"); }
    if(!checkTableExist("tests")){ sqlitedb->execute("CREATE TABLE tests (id INT PRIMARY KEY autoincrement, name VARCHAR(50), file VARCHAR(255), status INT)"); }
}

bool DBManager::checkTableExist(const QString &tablename){
    QSqlQuery result = sqlitedb->execute("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='"+tablename+"';");
    if(result.size()>0){
        return true;
    }else{
        return false;
    }
}

int DBManager::getSequence(const QString &tablename){
    QSqlQuery result = sqlitedb->execute("SELECT COUNT(*) FROM "+tablename);
    return result.size()+1;
}

bool DBManager::checkExistingTests(const QString &name, const QString &file){
    QSqlQuery result = sqlitedb->execute("SELECT COUNT(*) FROM tests WHERE name='"+name+"' AND file='"+file+"'; ");
    if(result.size()>0){
        return true;
    }else{
        return false;
    }
}

bool DBManager::addTests(const QString &name, const QString &file){
    QSqlQuery result = sqlitedb->execute("INSERT INTO tests ('name','file') VALUES ('"+name+"','"+file+"'); ");
    if(result.isValid()){
        qDebug("Y");
        return true;
    }else{
        qDebug("N");
        return false;
    }
}



/****************************************************************************
** Meta object code from reading C++ file 'mtsplitter.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mtsplitter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mtsplitter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MTSplitter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   12,   11,   11, 0x0a,
      44,   34,   11,   11, 0x0a,
      66,   11,   11,   11, 0x0a,
      77,   11,   11,   11, 0x0a,
      89,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MTSplitter[] = {
    "MTSplitter\0\0pos\0moveSplitter(int)\0"
    "pos,index\0moveSplitter(int,int)\0"
    "moveLeft()\0moveRight()\0moveToCentre()\0"
};

void MTSplitter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MTSplitter *_t = static_cast<MTSplitter *>(_o);
        switch (_id) {
        case 0: _t->moveSplitter((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->moveSplitter((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->moveLeft(); break;
        case 3: _t->moveRight(); break;
        case 4: _t->moveToCentre(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MTSplitter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MTSplitter::staticMetaObject = {
    { &QSplitter::staticMetaObject, qt_meta_stringdata_MTSplitter,
      qt_meta_data_MTSplitter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MTSplitter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MTSplitter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MTSplitter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MTSplitter))
        return static_cast<void*>(const_cast< MTSplitter*>(this));
    return QSplitter::qt_metacast(_clname);
}

int MTSplitter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSplitter::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

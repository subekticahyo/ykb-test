/****************************************************************************
** Meta object code from reading C++ file 'main_window.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../main_window.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'main_window.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      32,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      22,   11,   11,   11, 0x08,
      30,   11,   11,   11, 0x08,
      49,   11,   11,   11, 0x08,
      60,   11,   11,   11, 0x08,
      68,   11,   11,   11, 0x08,
      79,   11,   11,   11, 0x08,
      90,   11,   11,   11, 0x08,
     105,   11,   11,   11, 0x08,
     120,   11,   11,   11, 0x08,
     142,   11,   11,   11, 0x08,
     161,   11,   11,   11, 0x08,
     195,   11,   11,   11, 0x08,
     222,   11,   11,   11, 0x08,
     250,   11,   11,   11, 0x08,
     266,   11,   11,   11, 0x08,
     285,   11,   11,   11, 0x08,
     328,   11,   11,   11, 0x08,
     346,   11,   11,   11, 0x08,
     357,   11,   11,   11, 0x08,
     375,   11,   11,   11, 0x08,
     401,   11,   11,   11, 0x08,
     414,   11,   11,   11, 0x08,
     423,   11,   11,   11, 0x08,
     437,   11,   11,   11, 0x08,
     458,   11,   11,   11, 0x08,
     485,   11,   11,   11, 0x08,
     495,   11,   11,   11, 0x08,
     516,   11,   11,   11, 0x08,
     531,   11,   11,   11, 0x08,
     546,   11,   11,   11, 0x08,
     585,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0varinit()\0about()\0"
    "errorInvalidData()\0getReady()\0start()\0"
    "browse_i()\0browse_o()\0loadSettings()\0"
    "saveSettings()\0enableConnectButton()\0"
    "enableLoadButton()\0toggleInputType(QAbstractButton*)\0"
    "enableInputTypeSelection()\0"
    "disableInputTypeSelection()\0connectSocket()\0"
    "readIncomingData()\0"
    "displayError(QAbstractSocket::SocketError)\0"
    "loadTest(QString)\0loadFile()\0"
    "loadFile(QString)\0randomlySelectQuestions()\0"
    "updateTime()\0finish()\0sendResults()\0"
    "readResults(QString)\0loadResults(QTableWidget*)\0"
    "newTest()\0setCurrentQuestion()\0"
    "nextQuestion()\0lastQuestion()\0"
    "setQuestionAnswered(Question::Answers)\0"
    "previewSvg(QString)\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->varinit(); break;
        case 1: _t->about(); break;
        case 2: _t->errorInvalidData(); break;
        case 3: _t->getReady(); break;
        case 4: _t->start(); break;
        case 5: _t->browse_i(); break;
        case 6: _t->browse_o(); break;
        case 7: _t->loadSettings(); break;
        case 8: _t->saveSettings(); break;
        case 9: _t->enableConnectButton(); break;
        case 10: _t->enableLoadButton(); break;
        case 11: _t->toggleInputType((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 12: _t->enableInputTypeSelection(); break;
        case 13: _t->disableInputTypeSelection(); break;
        case 14: _t->connectSocket(); break;
        case 15: _t->readIncomingData(); break;
        case 16: _t->displayError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 17: _t->loadTest((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 18: _t->loadFile(); break;
        case 19: _t->loadFile((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 20: _t->randomlySelectQuestions(); break;
        case 21: _t->updateTime(); break;
        case 22: _t->finish(); break;
        case 23: _t->sendResults(); break;
        case 24: _t->readResults((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 25: _t->loadResults((*reinterpret_cast< QTableWidget*(*)>(_a[1]))); break;
        case 26: _t->newTest(); break;
        case 27: _t->setCurrentQuestion(); break;
        case 28: _t->nextQuestion(); break;
        case 29: _t->lastQuestion(); break;
        case 30: _t->setQuestionAnswered((*reinterpret_cast< Question::Answers(*)>(_a[1]))); break;
        case 31: _t->previewSvg((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 32)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 32;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql>
#include <QApplication>
#include <QDir>
#include <QString>

class SqliteDatabase
{
private:
    QSqlDatabase database;

public:
    SqliteDatabase();
    ~SqliteDatabase();

    // Executes a query and returns the results set.
    QSqlQuery execute(QString query);

    // Executes a query and prints the results.
    void executeAndPrint(QString query);
};

#endif // DATABASE_H

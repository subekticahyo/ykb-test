/********************************************************************************
** Form generated from reading UI file 'about_widget_v2.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUT_WIDGET_V2_H
#define UI_ABOUT_WIDGET_V2_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AboutWidget
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *vboxLayout;
    QTabWidget *tabWidget;
    QWidget *aboutTab;
    QGridLayout *gridLayout1;
    QTextBrowser *aboutTextBrowser;
    QWidget *aboutQtTab;
    QGridLayout *gridLayout2;
    QTextBrowser *aboutQtTextBrowser;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QPushButton *btnClose;

    void setupUi(QWidget *AboutWidget)
    {
        if (AboutWidget->objectName().isEmpty())
            AboutWidget->setObjectName(QString::fromUtf8("AboutWidget"));
        AboutWidget->resize(602, 303);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/contents.png"), QSize(), QIcon::Normal, QIcon::Off);
        AboutWidget->setWindowIcon(icon);
        gridLayout = new QGridLayout(AboutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(6, 6, 6, 6);
        vboxLayout = new QVBoxLayout();
        vboxLayout->setSpacing(6);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(AboutWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        aboutTab = new QWidget();
        aboutTab->setObjectName(QString::fromUtf8("aboutTab"));
        gridLayout1 = new QGridLayout(aboutTab);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        gridLayout1->setContentsMargins(6, 6, 6, 6);
        aboutTextBrowser = new QTextBrowser(aboutTab);
        aboutTextBrowser->setObjectName(QString::fromUtf8("aboutTextBrowser"));

        gridLayout1->addWidget(aboutTextBrowser, 0, 0, 1, 1);

        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/itest.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(aboutTab, icon1, QString());
        aboutQtTab = new QWidget();
        aboutQtTab->setObjectName(QString::fromUtf8("aboutQtTab"));
        gridLayout2 = new QGridLayout(aboutQtTab);
        gridLayout2->setObjectName(QString::fromUtf8("gridLayout2"));
        gridLayout2->setContentsMargins(6, 6, 6, 6);
        aboutQtTextBrowser = new QTextBrowser(aboutQtTab);
        aboutQtTextBrowser->setObjectName(QString::fromUtf8("aboutQtTextBrowser"));

        gridLayout2->addWidget(aboutQtTextBrowser, 0, 0, 1, 1);

        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/qt4.png"), QSize(), QIcon::Normal, QIcon::Off);
        tabWidget->addTab(aboutQtTab, icon2, QString());

        vboxLayout->addWidget(tabWidget);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        btnClose = new QPushButton(AboutWidget);
        btnClose->setObjectName(QString::fromUtf8("btnClose"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/fileclose.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnClose->setIcon(icon3);
        btnClose->setIconSize(QSize(16, 16));

        hboxLayout->addWidget(btnClose);


        vboxLayout->addLayout(hboxLayout);


        gridLayout->addLayout(vboxLayout, 0, 0, 1, 1);


        retranslateUi(AboutWidget);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(AboutWidget);
    } // setupUi

    void retranslateUi(QWidget *AboutWidget)
    {
        AboutWidget->setWindowTitle(QApplication::translate("AboutWidget", "About iTest", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(aboutTab), QApplication::translate("AboutWidget", "Tentang iTest", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(aboutQtTab), QApplication::translate("AboutWidget", "Tentang Qts", 0, QApplication::UnicodeUTF8));
        btnClose->setText(QApplication::translate("AboutWidget", "Tutup", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AboutWidget: public Ui_AboutWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUT_WIDGET_V2_H

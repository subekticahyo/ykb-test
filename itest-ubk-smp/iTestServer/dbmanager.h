#ifndef DBMANAGER_H
#define DBMANAGER_H

#include<QString>
#include"sqlite_database.h"

class DBManager
{
private:
    SqliteDatabase* sqlitedb ;

public:
    DBManager();
    void initializeDB();
    bool checkTableExist(const QString& tablename);

    bool checkExistingTests(const QString &name, const QString &file);
    bool addTests(const QString &name, const QString &file);

    int getSequence(const QString& tablename);

};

#endif // DBMANAGER_H

/****************************************************************************
** Meta object code from reading C++ file 'mttoolbutton.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mttoolbutton.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mttoolbutton.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MTToolButton[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      32,   13,   13,   13, 0x09,
      51,   13,   13,   13, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_MTToolButton[] = {
    "MTToolButton\0\0released(QString)\0"
    "updateProperties()\0emitReleased()\0"
};

void MTToolButton::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MTToolButton *_t = static_cast<MTToolButton *>(_o);
        switch (_id) {
        case 0: _t->released((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->updateProperties(); break;
        case 2: _t->emitReleased(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MTToolButton::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MTToolButton::staticMetaObject = {
    { &QToolButton::staticMetaObject, qt_meta_stringdata_MTToolButton,
      qt_meta_data_MTToolButton, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MTToolButton::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MTToolButton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MTToolButton::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MTToolButton))
        return static_cast<void*>(const_cast< MTToolButton*>(this));
    return QToolButton::qt_metacast(_clname);
}

int MTToolButton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void MTToolButton::released(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

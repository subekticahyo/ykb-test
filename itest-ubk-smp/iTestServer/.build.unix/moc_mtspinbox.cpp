/****************************************************************************
** Meta object code from reading C++ file 'mtspinbox.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mtspinbox.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mtspinbox.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MTSpinBox[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   11,   10,   10, 0x0a,
      33,   11,   10,   10, 0x0a,
      49,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MTSpinBox[] = {
    "MTSpinBox\0\0value\0setMinimum(int)\0"
    "setMaximum(int)\0setMaximumValue()\0"
};

void MTSpinBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MTSpinBox *_t = static_cast<MTSpinBox *>(_o);
        switch (_id) {
        case 0: _t->setMinimum((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->setMaximum((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->setMaximumValue(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MTSpinBox::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MTSpinBox::staticMetaObject = {
    { &QSpinBox::staticMetaObject, qt_meta_stringdata_MTSpinBox,
      qt_meta_data_MTSpinBox, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MTSpinBox::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MTSpinBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MTSpinBox::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MTSpinBox))
        return static_cast<void*>(const_cast< MTSpinBox*>(this));
    return QSpinBox::qt_metacast(_clname);
}

int MTSpinBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSpinBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_MTDoubleSpinBox[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,   17,   16,   16, 0x0a,
      42,   17,   16,   16, 0x0a,
      61,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MTDoubleSpinBox[] = {
    "MTDoubleSpinBox\0\0value\0setMinimum(double)\0"
    "setMaximum(double)\0setMaximumValue()\0"
};

void MTDoubleSpinBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MTDoubleSpinBox *_t = static_cast<MTDoubleSpinBox *>(_o);
        switch (_id) {
        case 0: _t->setMinimum((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->setMaximum((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->setMaximumValue(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MTDoubleSpinBox::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MTDoubleSpinBox::staticMetaObject = {
    { &QDoubleSpinBox::staticMetaObject, qt_meta_stringdata_MTDoubleSpinBox,
      qt_meta_data_MTDoubleSpinBox, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MTDoubleSpinBox::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MTDoubleSpinBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MTDoubleSpinBox::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MTDoubleSpinBox))
        return static_cast<void*>(const_cast< MTDoubleSpinBox*>(this));
    return QDoubleSpinBox::qt_metacast(_clname);
}

int MTDoubleSpinBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDoubleSpinBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

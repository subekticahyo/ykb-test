/****************************************************************************
** Meta object code from reading C++ file 'mttextedit.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mttextedit.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mttextedit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MTTextEdit[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      26,   11,   11,   11, 0x08,
      37,   11,   11,   11, 0x08,
      53,   11,   11,   11, 0x08,
      68,   66,   11,   11, 0x08,
      90,   88,   11,   11, 0x08,
     108,   11,   11,   11, 0x08,
     120,   11,   11,   11, 0x08,
     155,  148,   11,   11, 0x08,
     197,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MTTextEdit[] = {
    "MTTextEdit\0\0textChanged()\0textBold()\0"
    "textUnderline()\0textItalic()\0f\0"
    "textFamily(QString)\0p\0textSize(QString)\0"
    "textColor()\0textAlign(QAbstractButton*)\0"
    "format\0currentCharFormatChanged(QTextCharFormat)\0"
    "cursorPositionChanged()\0"
};

void MTTextEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MTTextEdit *_t = static_cast<MTTextEdit *>(_o);
        switch (_id) {
        case 0: _t->textChanged(); break;
        case 1: _t->textBold(); break;
        case 2: _t->textUnderline(); break;
        case 3: _t->textItalic(); break;
        case 4: _t->textFamily((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->textSize((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->textColor(); break;
        case 7: _t->textAlign((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 8: _t->currentCharFormatChanged((*reinterpret_cast< const QTextCharFormat(*)>(_a[1]))); break;
        case 9: _t->cursorPositionChanged(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MTTextEdit::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MTTextEdit::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MTTextEdit,
      qt_meta_data_MTTextEdit, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MTTextEdit::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MTTextEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MTTextEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MTTextEdit))
        return static_cast<void*>(const_cast< MTTextEdit*>(this));
    return QWidget::qt_metacast(_clname);
}

int MTTextEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void MTTextEdit::textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE

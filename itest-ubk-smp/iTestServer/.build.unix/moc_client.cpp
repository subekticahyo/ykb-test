/****************************************************************************
** Meta object code from reading C++ file 'client.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../client.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'client.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Client[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x05,
      28,    7,    7,    7, 0x05,
      46,    7,    7,    7, 0x05,
      69,    7,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
      91,    7,    7,    7, 0x0a,
     116,    7,  108,    7, 0x0a,
     123,    7,    7,    7, 0x0a,
     142,    7,  138,    7, 0x0a,
     151,    7,    7,    7, 0x0a,
     186,    7,  174,    7, 0x0a,
     202,    7,  195,    7, 0x0a,
     210,    7,  195,    7, 0x0a,
     230,    7,  225,    7, 0x0a,
     270,    7,  240,    7, 0x0a,
     280,    7,    7,    7, 0x0a,
     296,    7,  225,    7, 0x0a,
     305,    7,    7,    7, 0x0a,
     326,    7,    7,    7, 0x0a,
     347,    7,  225,    7, 0x0a,
     362,    7,    7,    7, 0x0a,
     405,    7,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Client[] = {
    "Client\0\0identified(Client*)\0"
    "finished(Client*)\0resultsLoaded(Client*)\0"
    "disconnected(Client*)\0setName(QString)\0"
    "QString\0name()\0setNumber(int)\0int\0"
    "number()\0setSocket(QTcpSocket*)\0"
    "QTcpSocket*\0socket()\0double\0score()\0"
    "maximumScore()\0bool\0isReady()\0"
    "QMap<QString,QuestionAnswer>*\0results()\0"
    "setPassed(bool)\0passed()\0loadResults(QString)\0"
    "readClientFeedback()\0isIdentified()\0"
    "displayError(QAbstractSocket::SocketError)\0"
    "socketDisconnected()\0"
};

void Client::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Client *_t = static_cast<Client *>(_o);
        switch (_id) {
        case 0: _t->identified((*reinterpret_cast< Client*(*)>(_a[1]))); break;
        case 1: _t->finished((*reinterpret_cast< Client*(*)>(_a[1]))); break;
        case 2: _t->resultsLoaded((*reinterpret_cast< Client*(*)>(_a[1]))); break;
        case 3: _t->disconnected((*reinterpret_cast< Client*(*)>(_a[1]))); break;
        case 4: _t->setName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: { QString _r = _t->name();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 6: _t->setNumber((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: { int _r = _t->number();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 8: _t->setSocket((*reinterpret_cast< QTcpSocket*(*)>(_a[1]))); break;
        case 9: { QTcpSocket* _r = _t->socket();
            if (_a[0]) *reinterpret_cast< QTcpSocket**>(_a[0]) = _r; }  break;
        case 10: { double _r = _t->score();
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = _r; }  break;
        case 11: { double _r = _t->maximumScore();
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = _r; }  break;
        case 12: { bool _r = _t->isReady();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: { QMap<QString,QuestionAnswer>* _r = _t->results();
            if (_a[0]) *reinterpret_cast< QMap<QString,QuestionAnswer>**>(_a[0]) = _r; }  break;
        case 14: _t->setPassed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: { bool _r = _t->passed();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 16: _t->loadResults((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 17: _t->readClientFeedback(); break;
        case 18: { bool _r = _t->isIdentified();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 19: _t->displayError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 20: _t->socketDisconnected(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Client::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Client::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Client,
      qt_meta_data_Client, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Client::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Client::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Client::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Client))
        return static_cast<void*>(const_cast< Client*>(this));
    return QObject::qt_metacast(_clname);
}

int Client::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void Client::identified(Client * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Client::finished(Client * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Client::resultsLoaded(Client * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Client::disconnected(Client * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE

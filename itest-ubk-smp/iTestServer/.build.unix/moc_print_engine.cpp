/****************************************************************************
** Meta object code from reading C++ file 'print_engine.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../print_engine.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'print_engine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PrintQuestionsDialogue[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   23,   23,   23, 0x08,
      45,   23,   23,   23, 0x08,
      69,   23,   23,   23, 0x08,
      94,   23,   23,   23, 0x08,
     133,   23,   23,   23, 0x08,
     154,   23,   23,   23, 0x08,
     175,   23,   23,   23, 0x08,
     192,   23,   23,   23, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_PrintQuestionsDialogue[] = {
    "PrintQuestionsDialogue\0\0addQuestionToPrint()\0"
    "removeQuestionToPrint()\0"
    "addAllQuestionsToPrint()\0"
    "togglePrintSelection(QAbstractButton*)\0"
    "togglePrintEnabled()\0resetDefaultValues()\0"
    "updateTestQnum()\0printQuestions()\0"
};

void PrintQuestionsDialogue::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PrintQuestionsDialogue *_t = static_cast<PrintQuestionsDialogue *>(_o);
        switch (_id) {
        case 0: _t->addQuestionToPrint(); break;
        case 1: _t->removeQuestionToPrint(); break;
        case 2: _t->addAllQuestionsToPrint(); break;
        case 3: _t->togglePrintSelection((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 4: _t->togglePrintEnabled(); break;
        case 5: _t->resetDefaultValues(); break;
        case 6: _t->updateTestQnum(); break;
        case 7: _t->printQuestions(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PrintQuestionsDialogue::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PrintQuestionsDialogue::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_PrintQuestionsDialogue,
      qt_meta_data_PrintQuestionsDialogue, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PrintQuestionsDialogue::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PrintQuestionsDialogue::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PrintQuestionsDialogue::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PrintQuestionsDialogue))
        return static_cast<void*>(const_cast< PrintQuestionsDialogue*>(this));
    return QWidget::qt_metacast(_clname);
}

int PrintQuestionsDialogue::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
